package com.rlgino.springci;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.rlgino.springci.config.AppConfiguration;

public class ConstructorInjectionTest {
    @Test
    public void processMsgTest() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfiguration.class);
        ConstructorInjection bean = applicationContext.getBean(ConstructorInjection.class);
        bean.processMsg("Gino");
        assertTrue(true);
        ((ConfigurableApplicationContext)applicationContext).close();
    }
    
}
