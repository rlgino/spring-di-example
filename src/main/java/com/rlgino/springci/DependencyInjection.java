package com.rlgino.springci;

public interface DependencyInjection {
    public void processMsg(String name);
}
