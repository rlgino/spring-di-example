package com.rlgino.springci;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCiApplication implements CommandLineRunner {
    
    @Autowired 
    @Qualifier("SetterInjection")
    DependencyInjection bean;

	public static void main(String[] args) {
		SpringApplication.run(SpringCiApplication.class, args);
	}

    @Override
    public void run( String... args) throws Exception {
        bean.processMsg("Gino");
    }

}
