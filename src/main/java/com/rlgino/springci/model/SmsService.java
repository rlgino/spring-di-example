package com.rlgino.springci.model;

import org.springframework.stereotype.Service;

@Service("SmsService")
public class SmsService implements MessageService{

    @Override
    public void sendMsg( String msg) {
        System.out.println(String.format("SMS dice: %s", msg));
    }    
}
