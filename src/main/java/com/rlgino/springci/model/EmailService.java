package com.rlgino.springci.model;

import org.springframework.stereotype.Service;

@Service("EmailService")
public class EmailService implements MessageService{

    @Override
    public void sendMsg( String msg) {
        System.out.println(String.format("Email dice: %s", msg));
    }    
}
