package com.rlgino.springci.model;

public interface MessageService {
    public void sendMsg(String msg);
}
