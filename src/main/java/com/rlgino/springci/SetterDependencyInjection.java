package com.rlgino.springci;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.rlgino.springci.model.MessageService;

@Component
@Qualifier("SetterInjection")
public class SetterDependencyInjection implements DependencyInjection {
    MessageService service;
    
    @Autowired
    @Qualifier( "EmailService" )
    public void setService( MessageService service) {
        this.service = service;
    }
    
    @Override
    public void processMsg( String name) {
        service.sendMsg(String.format("Ahora te saluda %s", name));
    }
    
}
