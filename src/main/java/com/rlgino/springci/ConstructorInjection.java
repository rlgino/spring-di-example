package com.rlgino.springci;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.rlgino.springci.model.MessageService;

@Component
@Qualifier("ConstructorInjection")
public class ConstructorInjection implements DependencyInjection {
    MessageService service;

    @Autowired 
    public ConstructorInjection(@Qualifier("TwitterService") MessageService service) {
        super();
        this.service = service;
    }
    
    public void processMsg(String name) {
        this.service.sendMsg(String.format("Hola %s", name));
    }
}
