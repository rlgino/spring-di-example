# Inyección de dependencia con Spring Framework

### Creación del proyecto
Se crea el proyecto en start.spring.io para iniciar un proyecto simple, y posteriormente se agrega el CommandLineRunner al main.
Para inyectar el bean, agregamos la anotación autowired y lo referenciamos a la interface que pronto se implementará en los diferentes métodos de inyección.
Despues de implementado el método se agrega al método 'run(...)' la implementación.

```java
 ... implements CommandLineRunner {
 	 @Autowired 
    DependencyInjection bean;
    ...
    @Override
    public void run( String... args) throws Exception {
        bean.processMsg("Gino");
    }
```

### Descripción del patrón
The Spring Framework Inversion of Control (IoC) component is the nucleus of the framework. It uses dependency injection to assemble Spring-provided (also called infrastructure components) and development-provided components in order to rapidly wrap up an application.

### Ventajas de la inyección de dependencias
Las ventajas de este patrón es:

* Bajo acoplamiento de componente

* Separación de responsabilidades

* Configuración y código separados

* Elección de diferente código sin depender de la implementación del mismo

* Mejoras en las pruebas

* DI permite que se utilicen elementos Mock más fácilmente.

## Explicación de Constructor
En este caso, la inyección se inicia en el constructor, para ver esto ver la clase ConstructorInjection

```java
 	@Autowired 
   public ConstructorInjection(@Qualifier("TwitterService") MessageService service) {
        super();
        this.service = service;
   }
```


## Explicación de Setter
En este caso, la inyección se inicia en el setter de la implementación, para ver esto ver la clase SetterDependencyInjection

```java
	 @Autowired
    @Qualifier( "EmailService" )
    public void setService( MessageService service) {
        this.service = service;
    }
```

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.2.RELEASE/maven-plugin/)
